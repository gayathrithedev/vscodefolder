import React, {useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';

const Child = ({currentFolder}) => {
  const [openFolder, setOpenFolder] = useState(false);
  const {title, children} = currentFolder;
  return (
    <>
      <TouchableOpacity
        style={[styles.button, styles.parent]}
        title="folder"
        onPress={() => setOpenFolder(!openFolder)}>
        <View style={styles.folderTitleWrapper}>
          {children.length !== 0 ? (
            <Text style={openFolder ? styles.arrowDown : styles.arrow}>></Text>
          ) : null}

          <Text
            style={
              openFolder
                ? [styles.folderText, styles.folderTitle]
                : styles.folderText
            }>
            {title}
          </Text>
        </View>
      </TouchableOpacity>
      {openFolder &&
        currentFolder.children.map((item) => (
          <Child currentFolder={item} key={item.title} />
        ))}
    </>
  );
};

const RootFolder = ({data}) => {
  const [openFolder, setOpenFolder] = useState(false);
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={[styles.button, styles.parent]}
        title="folder"
        onPress={() => setOpenFolder(!openFolder)}>
        <View style={styles.folderTitleWrapper}>
          <Text style={openFolder ? styles.arrowDown : styles.arrow}>></Text>
          <Text style={styles.folderText}>Folder</Text>
        </View>
      </TouchableOpacity>
      {openFolder &&
        data.map((item) => <Child currentFolder={item} key={item.title} />)}
    </View>
  );
};
export default RootFolder;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  button: {
    fontSize: 20,
    padding: 20,
    fontWeight: '400',
  },
  parent: {
    backgroundColor: '#121212',
  },
  child: {
    backgroundColor: '#121212',
    paddingLeft: 30,
  },
  folderText: {
    color: '#fff',
  },
  arrow: {
    marginRight: 10,
    color: 'white',
  },
  arrowDown: {
    color: 'white',
    marginRight: 10,
    transform: [
      {
        rotate: '60deg',
      },
    ],
  },
  folderTitleWrapper: {
    flexDirection: 'row',
  },
  folderTitle: {
    paddingLeft: 10,
  },
});
