import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const Folder = (props) => (
  <View style={styles.container}>
    <Text>Folder</Text>
  </View>
);
export default Folder;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
